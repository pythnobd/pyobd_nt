#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Senaryo kullanim ornegi
Bu komut dosyasinin adi, senaryo URL'sindekiyle tam olarak ayni olmali, ancak '.py' uzantisina sahip olmalidir.
URL    -    scm:scen_ecri_codevin#scen_appr_4valves_xxxxx.xml
'CALISTIR' proseduru pyren betigi tarafindan yurutulecektir
'''

import os, sys, re, time, string, mod_globals, mod_utils, mod_ecu, mod_db_manager,xml.dom.minidom
from mod_utils import Choice
from mod_utils import ASCIITOHEX
from mod_utils import StringToIntToHex
from mod_utils import pyren_encode
from mod_utils import clearScreen
from mod_utils import hex_VIN_plus_CRC
from collections import OrderedDict
from mod_utils import KBHit

reload(sys)
sys.setdefaultencoding('utf-8')

def run( elm, ecu, command, data ):
    '''
    Senaryonun ANA islevi
    Parametreler:
            elm         - adaptor sinifina referans
            ecu         - ecu sinifina referans
            command     - bu senaryonun gonderdigi komutlar
            data        - senaryo URL sindeki ilgili xml dosyasi adi ve parametreleri
    '''
    clearScreen()
    header =    '['+command.codeMR+'] '+command.label
    ScmSet = {}
    ScmParam = {}
    ScmName = {}

    def get_message( msg ):
        if msg in ScmParam.keys(): value = ScmParam[msg]
        else: value = msg
        if value.isdigit() and value in mod_globals.language_dict.keys(): value = pyren_encode( mod_globals.language_dict[value] )
        return value

    def get_message_by_id( id ):
        if id.isdigit() and id in mod_globals.language_dict.keys(): value = pyren_encode( mod_globals.language_dict[id] )
        return value
    
    kb = KBHit()
    
    DOMTree = xml.dom.minidom.parse(mod_db_manager.get_file_from_clip(data))
    ScmRoom = DOMTree.documentElement
    ScmParams = ScmRoom.getElementsByTagName("ScmParam")
    for Param in ScmParams:
        name = pyren_encode(Param.getAttribute("name"))
        value = pyren_encode(Param.getAttribute("value"))
        ScmParam[name] = value
    #negrsp = {'10' : NR10, '11' : NR11, '12' : NR12, '21' : NR21, '22' : NR22, '23' : NR23, '33' : NR33, '35' : NR35, '78' : NR78, '80' : NR80, 'FF' : NRFF}
    value1, datastr1 = ecu.get_id(ScmParam['ident_AVG_ETE'])
    value2, datastr2 = ecu.get_id(ScmParam['ident_AVD_ETE'])
    value3, datastr3 = ecu.get_id(ScmParam['ident_ARD_ETE'])
    value4, datastr4 = ecu.get_id(ScmParam['ident_ARG_ETE'])
    value5, datastr5 = ecu.get_id(ScmParam['ident_AVG_HIVER'])
    value6, datastr6 = ecu.get_id(ScmParam['ident_AVD_HIVER'])
    value7, datastr7 = ecu.get_id(ScmParam['ident_ARD_HIVER'])
    value8, datastr8 = ecu.get_id(ScmParam['ident_ARG_HIVER'])
    value9, datastr9 = ecu.get_st(ScmParam['EtatJeuRoue'])
    codeInfo = get_message_by_id('18953').decode('utf-8')
    codeRed = codeInfo.split('-')[1].strip()
    codeOrange = codeInfo.split('-')[2].strip()
    codeGreen = codeInfo.split('-')[3].strip()
    lastCodeStatus = ''
    tyreCodesTable = []
    summerTyreCodes = [value1,value2,value3,value4]
    winterTyreCodes = [value5,value6,value7,value8]    
    readCodeMessage = get_message_by_id('17884').decode('utf-8')
    confirm = get_message_by_id('19800').decode('utf-8')
    oneValveText = get_message_by_id('14964').decode('utf-8')
    fourValveText = get_message_by_id('14966').decode('utf-8')
    tyresLabels = []
    tyresLabels.append(get_message('LabelAVG_E1').decode('utf-8'))
    tyresLabels.append(get_message('LabelAVD_E1').decode('utf-8'))
    tyresLabels.append(get_message('LabelARD_E1').decode('utf-8'))
    tyresLabels.append(get_message('LabelARG_E1').decode('utf-8'))
    tariningCmdResp = ecu.Services[ecu.get_ref_cmd(ScmParam['ModeAppValves']).serviceID[0]].simpleRsp
    notUpdatedText = []
    notUpdatedText.append(get_message('TextTitre_E31').decode('utf-8'))
    notUpdatedText.append('*'*80)
    notUpdatedText.append(get_message_by_id('18954').decode('utf-8'))
    notUpdatedText.append('*'*80)
    notUpdatedText.append(datastr9)
    notUpdatedText.append('*'*80)
    searchInProgress = []
    searchInProgress.append(get_message('StatusAVG_C_E31').decode('utf-8'))
    searchInProgress.append(get_message('StatusAVD_C_E31').decode('utf-8'))
    searchInProgress.append(get_message('StatusARD_C_E31').decode('utf-8'))
    searchInProgress.append(get_message('StatusARG_C_E31').decode('utf-8'))
    searchFinished = []
    searchFinished.append(get_message('StatusAVG_T_E31').decode('utf-8'))
    searchFinished.append(get_message('StatusAVD_T_E31').decode('utf-8'))
    searchFinished.append(get_message('StatusARD_T_E31').decode('utf-8'))
    searchFinished.append(get_message('StatusARG_T_E31').decode('utf-8'))
    
    tyreCodesTable = []
    if get_message('TitreEte_E1') == pyren_encode(ecu.get_ref_st(ScmParam['EtatJeuRoue']).caracter[str(value9)]):
        summerTyresSet = True
        tyreCodesTable.append((datastr1))
        tyreCodesTable.append((datastr2))
        tyreCodesTable.append((datastr3))
        tyreCodesTable.append((datastr4))
    else:
        summerTyresSet = False
        tyreCodesTable.append((datastr5))
        tyreCodesTable.append((datastr6))
        tyreCodesTable.append((datastr7))
        tyreCodesTable.append((datastr8))
    notUpdatedText = notUpdatedText + tyreCodesTable
    notUpdatedText.append('*'*80)
    def readvalves():
        clearScreen()
        screen = generateScreen(get_message('STextTitre_E1').decode('utf-8'), [])
        print screen
        print '-'*80
        print datastr9
        print '-'*80
        print get_message('TexteInfo_E1').decode('utf-8')
        print '-'*80

    def generateScreen(title, codes):
        screen = ''
        screenPartOne = []
        screenPartOne.append(get_message('TextTitre_E1').decode('utf-8'))
        screenPartOne.append('*'*80)
        screenPartOne.append(title)
        screenPartOne.append('*'*80)
        integratedLines = []
        for num in range(len(tyreCodesTable)):
            try: integratedLines.append(tyreCodesTable[num] + codes[num])
            except: integratedLines.append(tyreCodesTable[num])
        integratedScreen = screenPartOne + integratedLines
        for lines in integratedScreen: screen = screen + pyren_encode(lines) + '\n'
        return screen

    def kodevales():
        if get_message('TitreEte_E1') == pyren_encode(ecu.get_ref_st(ScmParam['EtatJeuRoue']).caracter[str(value9)]):
            print '-'*80, '\n', datastr1, '\n', datastr2, '\n', datastr3, '\n', datastr4, '\n', '-'*80
        else:
            print '-'*80
            print datastr5
            print datastr6
            print datastr7
            print datastr8
            print '-'*80
    
    
    def writeCodes(codes, name):
        ch = raw_input(name.decode('utf-8') + ' <YES/NO>: ')
        while (ch.upper()!='YES') and (ch.upper()!='NO'):
            ch = raw_input(name.decode('utf-8') + ' <YES/NO>: ')
        if ch.upper()!='YES':
            print
            return
        print
        print '*'*80
        rsp = ecu.run_cmd(ScmParam['CommandeEcriture'], codes)
        print '*'*80
        print
    
    def oncreatevales():
        clearScreen
        selectedValve = ''
        selectedValveKey = ''
        screen = generateScreen(oneValveText, [])
        kodevales()
        valveLabelsDict = OrderedDict()
        for lb in range(4): valveLabelsDict[lb] = tyresLabels[lb]
        valveLabelsDict["exit"] = '< CIKIS >'
        clearScreen()
        print screen
        print '-'*80
        print datastr9
        print '-'*80
        choice = Choice(valveLabelsDict.values(), get_message_by_id('14127').decode('utf-8').replace('.',':'))
        clearScreen()
        print screen
        print '-'*80
        print datastr9
        print '-'*80
        for key, value in valveLabelsDict.iteritems():
            if choice[0] =='< CIKIS >': return
            if value == choice[0]:
                selectedValve = valveLabelsDict[key]
                selectedValveKey = key
        userCode = raw_input(selectedValve + ': ').upper()
        while not len(userCode) == 6 or not all(c in string.hexdigits for c in userCode):
            if not len(userCode) == 6: print 'VALF ID KODU 6 KARAKTER UZUNLUGUNDA OLMALIDIR.'
            else: print 'GiRDiGiNiZ VALF ID KODU GECERSiZ KARAKTER iCERiYOR'
            userCode = raw_input(selectedValve + ': ').upper()
        paramToSend = ''
        if summerTyresSet:
            for code in range(len(summerTyreCodes)):
                if code == selectedValveKey:paramToSend +=  userCode
                else:paramToSend +=  summerTyreCodes[code]
        else:
          for code in range(len(winterTyreCodes)):
                if code == selectedValveKey:paramToSend +=  userCode
                else:paramToSend +=  winterTyreCodes[code]
        clearScreen()
        print 
        print tyreCodesTable[selectedValveKey], userCode
        writeCodes(paramToSend, get_message_by_id('17571'))
    
    def fourcreatevales():
        userCodes = []
        screen = generateScreen(fourValveText, [])
        clearScreen()
        print screen
        print '*'*80
        ch = raw_input(confirm + ' <Y/N>: ')
        while (ch.upper()!='Y') and (ch.upper()!='N'): ch = raw_input(confirm + ' <Y/N>: ')
        if ch.upper()!='Y': return
        clearScreen()
        print screen
        for num in range(len(tyresLabels)):
            userCode = raw_input(tyresLabels[num] + ': ').upper()
            while not len(userCode) == 6 or not all(c in string.hexdigits for c in userCode):
                if not len(userCode) == 6: print 'VALF ID KODU 6 KARAKTER UZUNLUGUNDA OLMALIDIR.'
                else: print 'GiRDiGiNiZ VALF ID KODU GECERSiZ KARAKTER iCERiYOR'
                userCode = raw_input(tyresLabels[num] + ': ').upper()
            userCodes.append(userCode)
            clearScreen()
            screen = generateScreen(fourValveText, userCodes)
            print screen
        paramToSend = ''
        for code in userCodes: paramToSend += code
        writeCodes(paramToSend, get_message('TexteInfo_E5').replace('.', '?'))
    
    def sendTrainingCmd():
        resp = ecu.run_cmd(ScmParam['ModeAppValves'])
        clearScreen()
        if tariningCmdResp not in resp and not mod_globals.opt_demo:
            nrCode = resp[resp.find('NR') - 3: resp.find('NR')]
            print
            if 'NR' in resp:
                print negrsp[resp[resp.find('NR') - 3: resp.find('NR')]]
                print ''
            else:
                print negrsp['FF']
            print
            raw_input('CIKMAK iCiN HERHANGi BiR TUSA BASIN')
            return False
        else:
            return True

    def valvesTraining():
        readCodes = OrderedDict()
        if not sendTrainingCmd(): return
        tb = time.time()
        while(1):
            readCode = ecu.get_id(ScmParam['ident_EnCours'], 1)
            value2, datastr2 = ecu.get_st(ScmParam['EtatStatus'])
            oldScreen = ''
            for lines in notUpdatedText: oldScreen = oldScreen + pyren_encode(lines) + '\n'
            clearScreen()
            print oldScreen
            try: 
                print stateLabel + ': ' + searchInProgress[len(readCodes)]
            except:
                raw_input('4 LASTiK VALFi ID KODLARI OKUMA iSLEMi TAMAMLANDI.')
                return
            print
            print '*'*80
            if value2 == 3 and len(readCodes) < 4:
                print "%-50s %-20s"%(readCodeMessage, readCode)
                print
                print "VALF ID KODU OKUNAMADI"
            elif value2 == 2 and len(readCodes) < 4:
                print "%-50s %-20s"%(readCodeMessage, readCode)
                print
                if readCode != '000000' and readCode not in readCodes.keys():
                    if not readCode in tyreCodes: lastCodeStatus = codeRed
                    elif readCode == currentTyreSet[len(readCodes)]: lastCodeStatus = codeGreen
                    else: lastCodeStatus = codeOrange
                    readCodes[readCode] = readCode
                print lastCodeStatus
                print
                for code in range(len(readCodes)): print "%-60s %-8s"%(searchFinished[code], readCodes[readCodes.keys()[code]])
            print '*'*80
            print
            print 'CIKMAK iCiN HERHANGi BiR TUSA BASIN'
            print
            time.sleep(int(ScmParam['Val_Tempo_T2']) / 1000)
            tc = time.time()
            if (tc - tb) > int(ScmParam['Val_Tempo_T1']) / 1000:
                tb = time.time()
                if not sendTrainingCmd(): return
            if len(readCodes) == 4: break
            if kb.kbhit(): return
        print finishedInfo
        print
        paramToSend = ''
        for code in readCodes.keys(): paramToSend += code
        if "ERROR" in paramToSend:
            raw_input("VERi iNDiRME iSLEMi YANLIS GiTTi. iPTAL EDiLiYOR.")
            return
        writeCodes(paramToSend, get_message('TexteInfo_E5').replace('.', '?'))

    def chengecreate():
        clearScreen()
        print '*'*80
        print get_message('TextTitre_E3').decode('utf-8')
        print '*'*80
        print get_message('STextTitre_E3').decode('utf-8')
        print '*'*80
        print get_message('TexteInfo_E3').decode('utf-8')
        print '*'*80
        buttons = {}
        buttons[1] = oneValveText
        buttons[2] = fourValveText
        buttons[3] = get_message('STextTitre_E31').decode('utf-8')
        buttons["exit"] = '< CIKIS >'
        choiceC = Choice(buttons.values(), "SECiNiZ :")
        for key, value in buttons.iteritems():
            if choiceC[0] =='< CIKIS>': return
            if value == choiceC[0]:
                if key == 1: oncreatevales()
                elif key == 2: fourcreatevales()
                elif key == 3: valvesTraining()
                return
    
    def chengevales():
        clearScreen()
        print '*'*80
        print get_message('TextTitre_E2').decode('utf-8')
        print '*'*80
        print get_message('STextTitre_E2').decode('utf-8')
        print '*'*80
        print get_message('LabelSelect_E2').decode('utf-8')
        print '*'*80

    buttons = {}
    buttons[1] = get_message('STextTitre_E1').decode('utf-8')
    buttons[2] = get_message('STextTitre_E2').decode('utf-8')
    buttons[3] = get_message('STextTitre_E3').decode('utf-8')
    buttons["exit"] = '< CIKIS >'
    print '*'*80
    print get_message('TextTitre_E1')
    print '*'*80    
    choice = Choice(buttons.values(), "SECiNiZ :")
    for key, value in buttons.iteritems():
        if choice[0] =='< CIKIS>': return
        if value == choice[0]:
            if key == 1: readvalves()
            elif key == 2: print 'BU KISIM GELiSTiRME ASAMASINDADIR. BU AYAR CF001 PARAMETRESiNiN OLDUGU SATIR SECiLEREK YAPILABiLiR'#chengevales()
            elif key == 3: chengecreate()
            raw_input('CIKMAK iCiN HERHANGi BiR TUSA BASIN')
            return